import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Input a number: ");
        int nr1 = scanner.nextInt();
        System.out.println("Input another number: ");
        int nr2 = scanner.nextInt();

        if (nr1 > nr2) {
            System.out.println(nr1 + " is higher number than " + nr2);
        } else if (nr2 > nr1) {
            System.out.println(nr2 + " is a higher number than " + nr1);
        } else System.out.println("Numbers are equal");
    }

}
